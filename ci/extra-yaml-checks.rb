require 'yaml'
require 'find'
require 'resolv'
require 'English'

yaml_errors = []
yaml_warns = []

datadir, checktype = ARGV
abort("bad arguments: missing DATADIR") unless datadir and FileTest.directory?(datadir)
if checktype
  abort("bad arguments: wrong checktype [module|hostgroup]") unless checktype =~ /^(hostgroup|module)$/
else
  # guesswork ahead
  if FileTest.directory?("#{datadir}/hostgroup")
    checktype = "hostgroup"
  else
    checktype = "module"
  end
end

Find.find(datadir).each do |f|
  if FileTest.directory?(f)
    next
  end

  # filename == filetype checks
  case f
  when %r{(\.yaml$|/README.*|/\.git)}
    # ok
  else
    yaml_errors.push("bad filename \"#{f}\" - expect only *.yaml, .git*, README*")
    next
  end

  if checktype == 'hostgroup'
    # expect Hiera in either "fqdns/" (DNS-based resolution) or "hostgroup/"
    case f
    when %r{data/fqdns/(?<host>[^/]+)\.yaml}
      host = $LAST_MATCH_INFO[:host]
      if host.count('.') < 2
        yaml_errors.push("non-FQDN file \"#{f}\"")
        next
      end
      begin
        Resolv.getaddress(host)
      rescue
        yaml_warns.push("unresolved FQDN #{host} for file \"#{f}\"")
        next
      end
    when %r{data/hostgroup/(?<subhg>.*)\.yaml}
      subhg = $LAST_MATCH_INFO[:subhg]
      # something to check whether that #subhg exists in Foreman?
    else
      yaml_errors.push("YAML file \"#{f}\" in an unexpected tree (neither \"fqdns/\" nor \"hostgroup/\")")
      next
    end
  else
    # modules would have common.yaml, possibly $architecture.yaml, $OSFamily/$OSversion.yaml
    # - but some modify the search path and can have other directories or YAML files. shrug.
    case f
    when %r{^data/hostgroup/|^data/fqdn/}
      yaml_errors.push("YAML file \"#{f}\" in an unexpected (hostgroup-style) location")
    end
  end

  # file content checks
  data = YAML.load_file(f)
  if not data.class == Hash
    yaml_errors.push("Hiera file \"#{f}\" must contain a hash if it exists")
    next
  end
  data.each do | key, value |
    if key.is_a? String
      key.split("::").each do | component |
        if component.include?(':') or component.empty?
          yaml_errors.push("Malformed namespaced key \"#{key}\" in file \"#{f}\" found")
        end
      end
    else
      yaml_errors.push("Malformed yaml key (not a string): \"#{key}\"")
    end
  end
  # Check for multiple documents in file
  # while YAML.load_stream is the obvious choice to do this
  # it is not "safe" just split on ---
  documents = File.read(f).split(/^---\s*$/).reject!(&:empty?)
  if documents and documents.size > 1
    yaml_errors.push("Multiple (>1) yaml documents found in file \"#{f}\"")
  end
  #
end

yaml_warns.each {|e| print "WARN:", e, "\n"}

if not yaml_errors.empty?
  yaml_errors.each {|e| print "ERROR:", e, "\n"}
  abort "Found YAML errors."
else
  puts "Extra YAML checks passed."
end
