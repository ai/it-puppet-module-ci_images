#!/bin/bash
# look for RSPEC files that should end with "_rspec.rb" in order to be considered.
# try to ignore things that will just be included elsewhere.
if [[ -d code/spec ]]; then
	find code/spec -name '*_spec.rb' -o -name 'helper*.rb' -o \( -name '*.rb' -o -name '*.spec' \) -print0 |\
    xargs --null --no-run-if-empty grep -l "^describe" &&\
    { echo "ERROR: RSPEC testcases must end in '_spec.rb' to be used" >&1; exit 1; };
fi
true

