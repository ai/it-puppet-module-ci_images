it-puppet-module-ci_images is a dummy puppet module.

It contains next to no puppet code.

It does contain random thing that have caused problems so as to catch
them early.

e.g a copyright symbol - Copyright © 2023-2023 CERN


It does have actually run the docker code to create an image that all
other puppet cis then use.

One very important thing. When updating these files do update the date
string in the Dockerfile to stop the git clone result being cached at
docker level.



