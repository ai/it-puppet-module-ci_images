<table>
  <tr>
    <th><strong>Branch</strong></th>
    <th>Master</th>
    <th>QA</th>
    <th>Modulesync</th>
    <th>Coverage QA Branch</th>
  </tr>
  <tr>
    <td><strong>Build status</strong></td>
    <td>
      <a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/master'>
      <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/master/pipeline.svg'></a>
    </td>
    <td>
      <a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/qa'>
      <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/qa/pipeline.svg'></a>
    </td>
    <td>
      <a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/modulesync'>
      <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/modulesync/pipeline.svg'></a>
    </td>
    <td>
      <a href="https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/qa">
      <img alt="coverage report" src="https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/qa/coverage.svg" /></a>
    </td>
  </tr>
</table>

<br/>

Quick links:
* [Module readme](code/README.md) (may not exist)
* [Module reference](code/REFERENCE.md) (may not exist)
* [Module metadata](code/metadata.json)
* [Code directory](code/)
  * [Files directory](code/files)
  * [Libs directory](code/lib)
  * [Manifest directory](code/manifests)
  * [Templates directory](code/templates)
* [Data directory](data/)
* [Centrally maintained files for CI directory](ci/)

<br/>

Documentation and useful links:
* [CERN Configuration Guide](https://configdocs.web.cern.ch/configdocs/)
* [CERN modulesync configuration](https://gitlab.cern.ch/ai/it-puppet-modulesync-configs)
* [CERN modulesync changelog](https://gitlab.cern.ch/ai/it-puppet-modulesync-configs/blob/master/CHANGELOG.md)
* [Build results for all modules and hostgroups](https://gitlab.cern.ch/ai/it-puppet-modulesync-configs/blob/master/BUILDRESULTS.md)

* http://cern.ch

---

_This file is maintained with modulesync_
